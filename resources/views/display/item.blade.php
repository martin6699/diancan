@include('/layouts/header')
<h1 id=""></h1>
<h1 id="h02"></h1>

@if(count($value['item'])>0)
<form action = "/menu/select" id="selectID" method="post">
    {!! csrf_field() !!}
<select name="selectID">
    @for($i=0;$i<count($value['menu']);$i++)
    <option value="{!! $value['menu'][$i]['id'] !!}">{!! $value['menu'][$i]['restaurant_name'] !!}</option>
@endfor
</select>
    <input type="submit" value="确定"/>
</form>
<div>



            <table><thead><th>序号</th><th>名字</th><th>价格</th><th>开始时间</th><th>结束时间</th><th>修改</th><th>删除</th></thead>
                <tbody>
                @for($i=0;$i<count($value['item']);$i++)

                    <tr> <td>{!! $i+1 !!} </td>
                        <td><input id="n_{!! $value['item'][$i]['id']!!}" name="Name" type="text" value="{!! $value['item'][$i]['name'] !!}" /></td>
                        <td><input id="p_{!! $value['item'][$i]['id']!!}" name="Price" type="text" value="{!! $value['item'][$i]['price'] !!}" /></td>
                        <td><input id="s_{!! $value['item'][$i]['id']!!}" name="Start_at" type="text" value="{!! $value['item'][$i]['start_at'] !!}" /></td>
                        <td><input id="e_{!! $value['item'][$i]['id']!!}" name="End_at" type="text" value="{!! $value['item'][$i]['end_at'] !!}" /></td>
                        <td>
                            @can('update-item')
                            <a  href="/admin/item/{!! $value['item'][$i]['id']!!}/edit"     data-id="{!! $value['item'][$i]['id']!!}" class="mod" >修改</a>
                            @else
                                <a class="mod" style="color:gray">修改</a>
                            @endcan
                        </td>
                        <td>
                            @can('delete-item')
                            <button id="del{!! $value['item'][$i]['id']!!}" name="delButton" class="delButton">删除</button>
                            @else
                                <button name="noButton" class="delButton" disabled="true">删除</button>
                                @endcan
                        </td>
                    </tr>

                @endfor
                </tbody>
            </table>
        <div id="divhidden">{!! $value['item']->appends(['selectID'=>$value['selectID']])->render()   !!}</div>
    @else
        Not Data!



</div>

@endif




{{--<form action="/menu" id="menuID" method="post">

    <select name="selectID">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
    </select>

    <input type="submit"/>





</form>--}}


<div id="test99"></div>


<script type="text/javascript">
    $(function(){
        $("button").bind("click",function(){
            var $this = $(this);//获取当前元素对象;
            var $ID = $this.attr("id").slice(3);
            if($this.attr("id").slice(0,3)=="del") {
                //$("#test99").html($this.attr("id").slice(3));
                $.ajax({
                            url:"/admin/item/delete",
                            type:"post",//以post或者get方法异步请求,服务器端以request获取对象
                            beforeSend: function (xhr) {
                                var token = $('meta[name="csrf_token"]').attr('content');

                                if (token) {
                                    return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                                }
                            },
                            dataType:"text",// 期望返回的数据类型
                            data:{deleteID:$ID},//加到请求的数据
                            success:function(d){
                                $("#test99").html("success"+d);
                            }
                        }

                );

            }

            else
                alert("failure");
            //$.ajax("/admin/item/");


        });

    });

</script>

<style type="text/css">
    .pagination li
    {
        list-style: none;
        float:left;
        margin-right:10px;
    }



</style>
@include('/layouts/footer')