@include('/layouts/header')
<div><h3>添加角色</h3></div>
<div class="panel-body">
    <form name="addRole" id="addRole" action="/admin/author/storeRole"  method="post">
        {!! csrf_field() !!}
        <div class="form-group">
            <div class="input-group">
                <input class="form-control" name="name" id="roleID" type="text" disabled="true"
                        value=""  />
            </div>
        </div>
        <div class="form-group">
            <div class="input-group"><input class="form-control" name="label" id="label" type="text" disabled="true"
                                  value=""          /></div>
        </div>
        <div class="form-group">
            <div class="input-group"><input class="form-control" name="description" id="description" type="text" disabled="true"
                                            value=""         /></div>
        </div>
        <div>

        </div>


        <div class="permission-body" name="permissions">
            @for($i=0;$i<count($permissions);$i++)
                <div class="permission-group" name="group{!! $i !!}">
                    <h4>{!! $permissions[$i][0]['group'] !!}</h4>
                    @for($j=0;$j<count($permissions[$i]);$j++)
                        <span><input type="checkbox" name="permissions[]" value="{!! $permissions[$i][$j]['id'] !!}" @if($permissions[$i][$j]['checked']>0){!! checked  !!}@endif/>{!! $permissions[$i][$j]['description'] !!}</span>
                    @endfor
                </div>
            @endfor

        </div>

        <br/>

        <br/>
        <input type="submit" value="添加角色"/>

    </form>
</div>
<script type="text/css">
    ul li {list-style:none;}
</script>
@include('layouts/footer')