@include('/layouts/header')
<div><h1>添加权限成功</h1></div>

<div><h3>权限表</h3></div>
@if(count($permission)>0)
<table><thead><th>序号</th><th>名字</th><th>标签</th><th>介绍</th></thead>

    <tbody>
@for($i=0;$i<count($permission);$i++)
    <tr>
        <td>{!! $i+1 !!}</td>
        <td><a>{!! $permission[$i]['name'] !!}</a></td>
        <td><a>{!! $permission[$i]['label'] !!}</a></td>
        <td><a>{!! $permission[$i]['description'] !!}</a></td>
    </tr>
    @endfor
    </tbody>

    </table>
    @else
     Not Data!
@endif

<div><a href="/admin/permission/add">继续创建权限</a></div>
<div><a href="/admin/author/createRole">创建角色</a></div>


@include('layouts/footer')