@include('/layouts/header')
<h2>用户信息表</h2>
<table>
    {!! csrf_field() !!}
    <thead>
    <th>序号</th>
    <th>用户名</th>
    <th>邮箱</th>
    <th>当前角色</th>
    <th>选取角色</th>
    <th>更新角色</th>
    <th>删除用户</th>
    </thead>
    <tbody id="user-table">
    @for($i=0;$i<count($users[0]);$i++)
        <tr id="tr_{!! $users[0][$i]['id'] !!}">
            <td class="number">{!! $i+1 !!}</td>
            <td><input id="n_{!! $users[0][$i]['id'] !!}" name="name" type="text" value="{!! $users[0][$i]['name'] !!}"
                       disabled="true"/></td>
            <td><input id="e_{!! $users[0][$i]['id']!!}" name="label" type="text" value="{!! $users[0][$i]['email'] !!}"
                       disabled="true"/></td>
            <td><input id="r_{!! $users[0][$i]['id']!!}" name="currentRole" type="text"
                       value="{!! $users[0][$i]['currentRole'] !!}" disabled="true"/></td>
            <td><select name="selectID" id="select_{!! $users[0][$i]['id'] !!}">
                    <option value=" ">&nbsp;&nbsp;</option>
                    @foreach($users[1] as $role)
                        <option value="{!! $role['id']!!}">{!! $role['name'] !!}</option>
                    @endforeach
                </select></td>
            <td class="updateTd">
                <button id="update{!! $users[0][$i]['id']!!}" name="updateButton" class="updateButton">更新</button>
            </td>
            <td class="delTd">
                <button id="del{!! $users[0][$i]['id']!!}" name="delButton" class="delButton">删除</button>
            </td>
        </tr>
    @endfor

    </tbody>
</table>
<div><input type="submit" value="全部更新"/></div>

<script type="text/javascript">
    //javascript 函数内部变量可以访问外部全局变量;javascript具有自动垃圾回收机制,一旦数据不再使用,可将其设置为null来释放引用;
    $(function () {
        $(".updateTd").on("click", "button", function () {

            if ($(this).attr('id').slice(0, 6) == "update") {
                var $ID = $(this).attr('id').slice(6);
                var $selectID = "#select_" + $ID;
                var $selectedValue = $($selectID).val();
                var $selectedName = $($selectID).find("option:selected").text();
                var $currentID = $("#r_"+$ID).val();
                //当前角色与更新角色相同,停止更新;
                if($currentID==$selectedName)
                {
                    exit;
                }
                var $dataJson = {'userID':$ID, 'selectID':$selectedValue};

                $.ajax(
                        {
                            url: "/admin/user/addRoleToUser",
                            type: "post",
                            beforeSend: function (xhr) {
                                var token = $('meta[name="csrf_token"]').attr('content');

                                if (token) {
                                    return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                                }
                            },
                            dataType: "text",
                            data: $dataJson,

                            success: function ($value) {
                                if ($value != 0) {
                                    var $currentRole = "#r_" + $ID;
                                    $($currentRole).val($selectedName);//更新当前用户角色


                                    alert("更新成功!!!当前角色为"+$selectedName);
                                }
                                else {
                                    alert("更新失败!!!");
                                }

                            }

                        }
                );


            }


        });

        $(".delTd").on("click", "button", function () {
            var $ID = $(this).attr('id').slice(3);
            var $result = confirm("是否删除" + $("#n_"+$ID).val() + "用户?");
            if ($result == true) {
                if ($(this).attr('id').slice(0, 3) == "del") {
                    var currentID = $("tr_"+$ID)
                    $.ajax(
                            {
                                url: "/admin/user/delete",
                                type: "post",
                                beforeSend: function (xhr) {
                                    var token = $('meta[name="csrf_token"]').attr('content');

                                    if (token) {
                                        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                                    }
                                },
                                dataType: "text",
                                data: {deleteID: $ID},
                                success: function ($value) {
                                    if ($value == 1) {
                                        var $trID = "#tr_" + $ID;
                                        $($trID).empty();//移除该列表的行;
                                        $("#user-table>tr>td.number").map(function(key, item){
                                            $(item).html(key + 1);
                                        })
                                        alert("删除成功");

                                    }
                                    else {
                                        alert('删除失败');
                                    }
                                }
                            }
                    );
                }
            }


        });
    });
</script>
@include('layouts/footer')
