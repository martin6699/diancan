@include('/layouts/header')
<div><h1>创造权限</h1></div>
<div class="">
    <form name="createPermission" action="/admin/permission/add" method="post">
        {!! csrf_field() !!}
        <div class="form-group">
            <div class="input-group">
                <input class="form-control" name="name" id='permissionName' type="text" placeholder="权限名称,英文名"/>
            </div>
        </div>


        <div class="form-group">
            <div class="input-group">
                <input class="form-control" name="label" id='permissionLabel' type="text" placeholder="权限标签"/>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <input class="form-control" name="description" id='permissionDescrition' type="text" placeholder="详情"/>
            </div>
        </div>
        <input type="submit" value="提交"/>

    </form>

</div>
@include('layouts/footer')