@include('/layouts/header')
<h2>角色表</h2>
@if(count($roles)>0)
<table><thead><th>序号</th><th>角色名称</th><th>标签</th><th>详情</th><th>修改</th><th>删除</th></thead>
    <tbody>
    @for($i=0;$i<count($roles);$i++)

        <tr> <td>{!! $i+1 !!} </td>
            <td><input id="n_{!! $roles[$i]['id'] !!}" name="name" type="text" value="{!! $roles[$i]['name'] !!}" disabled="true" /></td>
            <td><input id="l_{!! $roles[$i]['id']!!}" name="label" type="text" value="{!! $roles[$i]['label'] !!}" disabled="true" /></td>
            <td><input id="d_{!! $roles[$i]['id']!!}" name="description" type="text" value="{!! $roles[$i]['description'] !!}" disabled="true" /></td>
            <td><a  href="/admin/author/{!! $roles[$i]['id']!!}/edit"     data-id="{!! $roles[$i]['id']!!}" class="mod" >修改</a></td>
            <td><button id="del{!! $roles[$i]['id']!!}" name="delButton" class="delButton">删除</button></td>
        </tr>

    @endfor
    </tbody>
</table>

    @else
    Not Data!
    @endif
<div><a href="/admin/user/displayUser" class="display">用户权限列表</a></div>
@include('layouts/footer')