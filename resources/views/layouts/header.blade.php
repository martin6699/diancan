<!DOCTYPE html>
<html>
<head>
    <meta name="csrf_token" content="{{ csrf_token() }}" />
    <script type="text/javascript" src="/js/jquery-2.1.4.js"></script>
<style type="text/css">
    #menuNav {
        font-family: Arial;
        font-size: 14px;
        width: 100%;
        overflow: hidden;

    }

    #menuNav, #menuNav ul {
        list-style-type: none;
        background: #A3C159;
        margin: 0;
        padding: 0;
        text-align:right;
    }

    #menuNav li {
        float: left;
    }

    #menuNav li a {
        display: block;
        padding: 10px 15px;
        color: #FFF;
        text-decoration: none;
        border-right: 1px solid #FFF;
    }
    #menuNav li a:hover {
        background: #1BA6B2;
    }

    #menuNav li ul li {
        float: none;
    }

    #menuNav li ul li a {
        border-top: 1px solid #FFF;
    }

    #menuNav li ul {
        display: none;
        position: absolute;
    }

    #menuNav li:hover ul {
        display: block;
    }
    .logout{
        text-align:right;
    }



    th label { margin:10px }

</style>
</head>
<body>
<h2 style="text-align:center">点餐系统</h2>
<div class="logout"><a href="">{{Auth::user()->name }}</a>&nbsp;&nbsp;<a href="/auth/logout" >logout</a></div>
<ul id="menuNav">
    <li>
        <a href="/item">首页</a>
        <ul>
            <li>
                <a href="/item">本月餐单</a>
            </li>
            <li>
                <a href="#">上月餐单</a>
            </li>

        </ul>
    </li>
    <li>
        <a href="#">餐单管理</a>
        <ul>
            <li>
                <a href="/admin/menu/create">创建餐单</a>
            </li>
            <li>
                <a href="/admin/menu/show">修改餐单</a>
            </li>
            <li>
                <a href="/item">修改菜肴</a>
            </li>
            <li>
                <a href="/admin/menu/delete">删除餐单</a>
            </li>

        </ul>
    </li>
    <li>
        <a href="/user/menu/show">点餐管理</a>

    </li>
    <li>
        <a href="#">点餐统计</a>
        <ul>
            <li>
                <a href="/user/temp/order/show">今点餐表</a>
            </li>
            <li>
                <a href="/user/all/order/show">点餐汇总</a>
            </li>
            <li>
                <a href="#">总点餐表</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#">权限管理</a>
        <ul>
            <li>
                <a href="/admin/permission/add">添加权限&nbsp;&nbsp;&nbsp;&nbsp;</a>
            </li>
            <li>
                <a href="/admin/role/add">添加角色&nbsp;&nbsp;&nbsp;&nbsp;</a>
            </li>
            <li>
                <a href="/admin/user/displayUser">更新用户权限</a>
            </li>
        </ul>
    </li>
</ul>

