@include('/layouts/header')
<h4>修改菜单</h4>

    {!! csrf_field() !!}
    <table><thead><th>菜单</th><th>电话</th><th>网址</th><th>开始时间</th><th>结束时间</th><th>添加菜肴</th><th>删除</th></thead>
        <tbody>
        @for($i=0;$i<count($value);$i++)
        <tr>
            <th><label>{!! $value[$i]['restaurant_name'] !!}</label></th>
            <th><label>{!! $value[$i]['phone'] !!}</label></th>
            <th><label>{!! $value[$i]['picture_url'] !!}</label></th>
            <th><label>{!! $value[$i]['start_at'] !!}</label></th>
            <th><label>{!! $value[$i]['end_at'] !!}</label></th>
            <th><a   class="mod"  href="/admin/menu/{!! $value[$i]['id'] !!}/edit" >添加</a></th>
            <th><input class="del" id="del{!! $value[$i]['id'] !!}"  type="button" value="删除"/></th>
        </tr>
            @endfor
        </tbody>
    </table>

<script type="text/javascript">

        $(document).on("click","th input[type='button']",function(){
            var $this = $(this);//获取当前元素对象;
            var $ID = $this.attr("id").slice(3);
            if($this.attr("id").slice(0,3)=="del") {
                //$("#test99").html($this.attr("id").slice(3));
                $.ajax({
                            url:"/admin/menu/delete",
                            type:"post",//以post或者get方法异步请求,服务器端以request获取对象
                            beforeSend: function (xhr) {
                                var token = $('meta[name="csrf_token"]').attr('content');

                                if (token) {
                                    return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                                }
                            },
                            dataType:"text",// 期望返回的数据类型
                            data:{deleteID:$ID},//加到请求的数据
                            success:function(d){
                                $("#test99").html(d);
                            }
                        }

                );

            }

            else
                alert("failure");
            //$.ajax("/admin/item/");


        });


    </script>
<div id="test99"></div>
{!! \Illuminate\Support\Facades\Cookie::get('userName') !!}
@include('/layouts/footer')