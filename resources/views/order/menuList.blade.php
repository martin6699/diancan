@include('layouts.header')
<h4>菜单</h4>
<div class="all">
    <ul>
    @for($i=0;$i<count($values);$i++)

    <li class="menu">
        <div class="menu1">
        <a href="/user/menu/{!! $values[$i]['id'] !!}/ordering">
        <div class="name"><p>{!! $values[$i]['restaurant_name'] !!}</p></div>
        <div class="phone"><p>电话: {!! $values[$i]['phone'] !!}</p></div>
        <div class="url"><p>网址: {!! $values[$i]['picture_url'] !!}</p></div>
            <div class="date"><p>截止时间: {!! $values[$i]['start_at'] !!} -- {!! $values[$i]['end_at'] !!}</p></div>
        </a>
        </div>
        </li>
        @endfor
    </ul>
</div>
<style>
    h4 {margin: auto;text-align: center}
    .all {margin: auto;width:600px;}
    .all .menu .menu1{padding:0 0 0 3em}
    ul li {margin:10px;list-style: none;}

    ul li .menu1{ border:1px dashed #666666;width:400px;margin:10px;}
    a:link {color: blue}   /*未被访问的链接*/
    a:visited {color: blue}             /*已被访问过的链接 */
    a:hover {color: blue}            /* 鼠标悬浮在上的链接*/
    a:active {color: blue}    /*鼠标点中激活链接*/
</style>
@include('layouts.footer')