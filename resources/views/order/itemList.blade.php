@include('layouts.header')
<h4>开始点餐</h4>
<form id="" class="order" action="/user/menu/store" method="post">
    {{ csrf_field() }}
<table>
    <thead>
    <th>点餐人</th>
    <th>菜肴</th>
    <th>总价</th>
    <th>备选菜肴</th>
    <th>总价</th>
    <th>删除</th>
    </thead>
    <tbody>
    <tr>
        <td>{{ Auth::user()->name }}</td>
        <td><input class="item"  name="item1[]" id="a_{{ Auth::user()->getId() }}" data-id="1" type="text"/></td>
        <td><input class="price" name="a_p"/></td>
        <td><input class="item"  name="item1[]" id="b_{{ Auth::user()->getId() }}" data-id="2" type="text"/></td>
        <td><input class="price" name="b_p"/></td>
        <td><input class="name" name="del_{{ Auth::user()->getId() }}" type="button" value="删除"/></td>
    </tr>
    </tbody>

</table>
    <input name="userID" value="{{ Auth::user()->getId() }}" type="hidden" />
    <input name="menuID" value="{{ $values['menuID'] }}" type="hidden" />
<input class="submit" name="submitOrder" type="submit" value="提交"/>
</form>
<div id="bg"></div>
<div class="content">

    {{--<div class="top"><h2>菜肴选择<a href="#" class="close">关闭</a></h2></div>
     <div class="list">
         @foreach($values as $key => $items)
             <h4>{{ $key }}</h4>
             <ul name="c_{{ $items[0]['categoryId'] }}" class="category">
                 @for($i=0;$i<count($items);$i++)
                     <li>
                         <label><input name="item" type="checkbox" value="{{ $items[$i]['id'] }}" data-name="{{ $items[$i]['name'] }}" data-price="{{ $items[$i]['price'] }}"/>{{ $items[$i]['name'] }} ({{ $items[$i]['price'] }}元)</label>
                     </li>
                 @endfor
             </ul>
         @endforeach
     </div>
     <div><input class="confirm" name="confirm" value="确定"/></div>--}}

</div>

<script type="text/javascript">

    var $totalPrice = 0;
    var $totalItem = [];
    var $content = $(".content");
    var $id;
    $(function () {



        $("tr td").on("click", "input[name='item1[]']", function () {
            $id = $(this).attr("id");


            $("#bg").css("display","block");
            $(".content").css("display","block");
            $content.append("<div class='top'><h2>菜肴选择<a href='#' class='close'>关闭</a></h2></div>");

            var $list = $("<div class='list'></div>");
            $list.append("@foreach($values[0] as $key => $items)<h4>{{ $key }}</h4>");
            //$ul中后面</ul>不能放'endforeach'因为blade会在for后面检查是否有endfor,如果检查到endforeach就会报错;
            var $ul = $("<ul name='c_{{ $items[0]['categoryId'] }}' class='category'> " + "@for($i=0;$i<count($items);$i++)</ul>");
            $ul.append("<li><input name='item' type='checkbox' value='{{ $items[$i]['id'] }}' data-name='{{ $items[$i]['name'] }}' data-price='{{ $items[$i]['price'] }}''/>{{ $items[$i]['name'] }} ({{ $items[$i]['price'] }}元)</li>@endfor");
            $list.append($ul);
            $list.append("@endforeach");
            $content.append($list);
            $content.append("<div><input class='confirm' name='confirm' type='button' value='确定'/></div>");
            $content.append("<div class='showError'><h4></h4></div>");


            /* $("#bg").css("display","block");
             $(".content").css("display","block");*/


        });


        //判断所选菜肴是否超过指定金额;
        $(document).on("click", "ul li input[name='item']", function (){
            var $price = parseInt($(this).attr('data-price'));
            var $name = $(this).attr('data-name');
            if ($(this).is(":checked")) {


                if (($totalPrice + $price) > 25) {
                    $(".showError h4").html("总价不能超过25元").show(300).delay(3000).hide(300);
                    $(this).attr("checked", false);
                    exit();

                }
                else {
                    $totalPrice = $totalPrice + $price;
                    $totalItem.push($name);

                }
            }
            else {

                var $index = $totalItem.indexOf($name);
                $totalPrice = $totalPrice - $price;
                $totalItem.splice($index, 1);//array.splice()方法删除数组一部分成员;下标位置为删除的开始;

            }
        });


//
        $(document).on("click",".content div .confirm",function () {

            var itemId = "#"+$id;
            var $priceId ="input[name='"+$id.slice(0,2)+"p']";
            $(itemId).prop("value", " ");
            $($priceId).prop("value", " ");
            $totalItem = $totalItem.join("+");
            $(itemId).prop("value", $totalItem);
            $($priceId).prop("value", $totalPrice);
            $totalItem =[];
            $totalPrice = 0;
            $("#bg").css("display","none");
            $(".content").css("display","none");
            $content.empty();

        });

        $(document).on("click",".content .top h2 .close",function () {

            $("input[name='item']").attr("checked", false);
            $totalItem =[];
            $totalPrice = 0;
            $("#bg").css("display", "none");
            $(".content").css("display", "none");
            $content.empty();

        });


        $("tr td").on("click","",function(){


        });




    });

    //    function showWin(){
    //        /*找到div节点并返回*/
    //        var winNode = $("#win");
    //        //方法一：利用js修改css的值，实现显示效果
    //        // winNode.css("display", "block");
    //        //方法二：利用jquery的show方法，实现显示效果
    //        // winNode.show("slow");
    //        //方法三：利用jquery的fadeIn方法实现淡入
    //        winNode.fadeIn("slow");
    //    }
    //    function hide(){
    //        var winNode = $("#win");
    //        //方法一：修改css的值
    //        //winNode.css("display", "none");
    //        //方法二：jquery的fadeOut方式
    //        winNode.fadeOut("slow");
    //        //方法三：jquery的hide方法
    //        winNode.hide("slow");
    //    }
</script>

<style type="text/css">

    #bg {
        display: none;
        position: absolute;
        width: 100%;
        height: 100%;
        background: #666666;
        z-index: 2;
        top: 0;
        left: 0;
        opacity: 0.7;
    }

    .content {
        display: none;
        width: 1200px;
        height: 900px;
        position: absolute;
        top: 20%;
        margin-top: -150px;
        background: #fff;
        z-index: 3;
        left: 30%;
        margin-left: -50px;
    }

    .close {
        position: absolute;
        right: 0;
        top: 3px;
        font-size: 14px;
        color: #000000;
        corsor: pointer
    }

    .top h2 {
        text-align: center
    }

    .list ul li {
        list-style: none;
    }
    .item {width:200px;}
    .price { width:30px;}
</style>

@include('layouts.footer')