@include('layouts.header')
<h3>点餐列表</h3>

<table>
    <thead><th>序号</th><th>日期</th><th>餐单</th><th>名字</th><th>订单</th><th>价格</th><th>备选订单</th><th>价格</th><th>删除</th></thead>
@for($i=0;$i<count($values);$i++)
        <form class="form" method="post" action="/user/order/delete">
    <tr>
        <td>{{ $i+1 }}</td>
        <td><input class="" name="" type="text" value="{{ date("Y-m-d",strtotime($values[$i]['menu_time']))}}" disabled="true"/></td>
        <td><input class="" name="" type="text" value="{{ $values[$i]['menu_name'] }}" disabled="true" /></td>
        <td><input class="" name="" type="text" value="{{ $values[$i]['user_name'] }}" disabled="true" /></td>
        <td><input class="" name="" type="text" value="{{ $values[$i]['item_first'] }}" disabled="true" /></td>
        <td><input class="" name="" type="text" value="{{ $values[$i]['price_first'] }}" disabled="true" /></td>
        <td><input class="" name="" type="text" value="{{ $values[$i]['item_second'] }}" disabled="true" /></td>
        <td><input class="" name="" type="text" value="{{ $values[$i]['price_second'] }}" disabled="true" /></td>

        <td><input class="del" name="del{{$values[$i]['id']}}" type="submit"
                   @can('admin') @else
                   @if($values[$i]['user_id']!= Auth::User()->getId()||$values[$i]['menu_time']!=Carbon\Carbon::today())
                   disabled @endif
                   @endcan
                   value="删除"/></td>
    </tr>
            <input type="hidden" name="orderId" value="{{$values[$i]['id']}}">
            {!! csrf_field() !!}
        </form>
    @endfor

</table>

<p id="test99"> </p>
<link rel="stylesheet" href="/css/tempOrder.css"/>
{{--<script type="text/javascript">
    $(function(){

        $("table tr td ").on('click',"input[class='del']",function(){
            $id = $(this).attr('name').slice(3);

            $.ajax({
                        url:"/user/order/delete",
                        type:"post",//以post或者get方法异步请求,服务器端以request获取对象
                        beforeSend: function (xhr) {
                            var token = $('meta[name="csrf_token"]').attr('content');

                            if (token) {
                                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        dataType:"text",// 期望返回的数据类型
                        data:{deleteID:$id},//加到请求的数据
                        error:function(XMLHttpRequest,textStatus,errorThrown)
                        {
                            $("#test99").html('XMLHttpRequest.status:'+XMLHttpRequest.status+';XMLHttpRequest.readyState:'+XMLHttpRequest.readyState+';XMLHttpRequest.textStatus:'+textStatus);
                        },
                        success:function(d){
                            $("#test99").html("success"+d);
                            parent.location.reload();
                        }
                    }

            );
        });

    });
</script>--}}
@include('layouts.footer')

