<!DOCTYPE html>
<html>
<head>
    <script type="text/css">
        input:invalid {
            background-color: #ffdddd;
        }

        input:valid {
            background-color: #ddffdd;
        }
    </script>
</head>
<body>
{{--<div class=""><a href="/auth/logout">logout</a></div>--}}
<div class="panel-body">
    <form name="userLogin" id="userLogin" action="/auth/login" method="post">
        {!! csrf_field() !!}
        <div class="form-group">
            <div class="input-group"><input class="form-control" name="email" id="email" type="email"
                                            placeholder="邮箱"/></div>
        </div>
        <div class="form-group">
            <div class="input-group"><input class="form-control" name="password" id="password" type="password"
                                            placeholder="密码"/></div>
        </div>
        <div class="remember"><input class="form-control" name="remember" id="remember" type="checkbox"  value='1'/>记住我</div>
        <input type="submit" value="提交"/>

    </form>
    {!! $errors->first('email') !!}
    {!! $errors->first('password') !!}
    @if(isset($messages)) <p> {!! $messages !!} </p> @endif


</div>

</body>
</html>