<!DOCTYPE html>
<html>
<head>
    <script type="text/css">
        input:invalid {
            background-color: #ffdddd;
        }

        input:valid {
            background-color: #ddffdd;
        }
    </script>
</head>
<body>
<div><h1>用户注册</h1></div>
<div class="panel-body">
    <form name="Register" id="Register" action="/auth/register" method="post">
       {!! csrf_field() !!}
        <div class="form-group">
            <div class="input-group">
                <input class="form-control" name="name" id="userID" type="text"
                       placeholder="用户名" value="{!! old('name') !!}"/>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group"><input class="form-control" name="email" id="email" type="email"
                                            placeholder="邮箱" value="{!! old('email') !!}"/></div>
        </div>
        <div class="form-group">
            <div class="input-group"><input class="form-control" name="password" id="password" type="password"
                                            placeholder="密码"/></div>
        </div>
        <div class="form-group">
            <div class="input-group"><input class="form-control" name="password_confirmation" id="password" type="password"
                                            placeholder="再次输入密码"/></div>
        </div>

        <input type="submit" value="提交"/>

    </form>
</div>

{!! $errors->first('name') !!}
{!! $errors->first('email') !!}
{!! $errors->first('password') !!}
</body>
</html>