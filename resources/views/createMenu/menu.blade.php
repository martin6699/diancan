@include('/layouts/header')
<h4>创建餐单</h4>

<form action="/admin/menu/create" id="createMenu"  method="post">
    {!! csrf_field() !!}
    <div><label>餐厅名</label>
        <input id="name" name="name" type="text"/>
        </div>
    <div><label>餐厅电话</label>
        <input id="phone" name="phone" type="text"/>
    </div>
    <div><label>餐单有效期</label>
        <input id="start_at" name="start_at" type="date" min="{!! date('Y-m') !!}" max="{!! date('Y-m') !!}"/>-
        <input id="end_at" name="end_at" type="date" min="{!! date('Y-m') !!}" max="{!! date('Y-m') !!}"/>
    </div>
    <div><label>餐厅网址</label>
        <input id="restUrl" name="restUrl" type="text"/>
    </div>
    <div>
        <input id="submit1" type="submit" value="提交"/>
    </div>
    </form>
<div><p>{!! $message !!}</p></div>
{{--<p>***********************************************************</p>
<h4>一键创餐单</h4>
<form action="/admin/menu/keyCreate" id="keycreate" method="post">
    <div><label>餐厅名</label>
        <input id="name2" name="name2" type="text"/>
    </div>
    <div><label>餐厅电话</label>
        <input id="phone2" name="phone2" type="text"/>
    </div>
    <div><label>餐单有效期</label>
        <input id="start_at2" name="start_at2" type="date" min="2016-01-01" max="{!! date('Y-m-d') !!}"/>-
        <input id="end_at2" name="end_at2" type="date" min="2016-01-01" max="{!! date('Y-m-d') !!}"/>
    </div>
    <div><label>餐厅网址</label>
        <input id="restUrl" name="restUrl" type="text"/>
    </div>
    <div>
        <input id="submit2" type="submit" value="提交"/>
    </div>
</form>--}}

@include('/layouts/footer')