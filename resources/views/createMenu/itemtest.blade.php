@include('/layouts/header')

<h4>创建菜肴</h4>
{{--<div>
<div><label>餐厅名</label><input name="name" type="text" value="{!! $value['menu']['name'] !!}" disabled="true"></div>
<div><label>联系电话</label><input name="phone" type="text" value="{!! $value['menu']['phone'] !!}" disabled="true"></div>
<div><label>餐单网址</label><input name="url" type="text" value="{!! $value['menu']['url'] !!}" disabled="true"></div>
<div><label>餐单开始时间</label><input name="start_at" type="text" value="{!! $value['menu']['start_at'] !!}" disabled="true"></div>
<div><label>餐单结束时间</label><input name="end_at" type="text" value="{!! $value['menu']['end_at'] !!}" disabled="true"></div>
</div>
<br/>--}}
<table id="TbData">
    <thead><th>序号</th><th>菜肴名</th><th>价格</th><th>分类</th><th>有效时间</th><th>有效时间段</th></thead>
    <tbody>

        <tr class="ClassRow" id="TrDataRow1">
            <td class="number"><label>1</label></td>
            <td><input name="itemName[]" type="text"/></td>
            <td><input name="itemPrice[]" type="text"/></td>
            <td>
                <select name="selectName[]" >
                    <option>&nbsp;&nbsp;</option>
                    @foreach($value['category'] as $category)
                    <option value="{!! $category['id']!!}">{!! $category['name'] !!}</option>
                        @endforeach
                </select>
            </td>
            <td><input id="fore_1" class="forever" name="forever[]" type="checkbox" onclick="stopSetTime(this)"/>长期有效<br/>
                <input id="temp_1" class="temp"  name="forever[]" type="checkbox" onclick="showSetTime(this)"/>限制时间</td>
            <td>
                <input id="s_1" class="startAt" name="item_start_at[]" type="date" min="{!! date('Y-m')  !!}" max="{!! date('Y-m') !!}"  onchange="changeTime(this.id)" disabled="true"/>-
                <input id="e_1" class="endAt" name="item_end_at[]" type="date" min="{!! date('Y-m') !!}" max="{!! date('Y-m') !!}"    onchange="changeTime(this.id)" disabled="true" />
            </td>
        </tr>
    </tbody>
    </table>
<div><input name="addItem" type="button" onclick="addItemList()" value="添加行"/></div>
<script type="text/javascript">

    function stopSetTime(obj)
    {
        var $foreverID = obj.id.slice(5);

        if(obj.checked==true)
        {
          $("#temp_"+$foreverID).attr('checked',false);
        }
        //var $foreverID = id.slice(5);
        $("#s_"+$foreverID).val(" ");
        $("#e_"+$foreverID).val(" ");
        $("#s_"+$foreverID).attr('disabled',true);
        $("#e_"+$foreverID).attr('disabled',true);
    }

    function showSetTime(obj)
    {
        var $tempID = obj.id.slice(5);

        if(obj.checked==true)
        {
            $("#fore_"+$tempID).attr('checked',false);
        }

      //var $tempID = id.slice(5);
        $("#s_"+$tempID).removeAttr('disabled');
        $("#e_"+$tempID).removeAttr('disabled');
    }

    function changeTime(id)
    {
     var $ID = id.slice(2);
        var $startValue = $("#s_"+$ID).val();
       /* var $startValueString = $startValue.toString()*/
        var $endValue = $("#e_"+$ID).val();

        if(($startValue)&&($endValue))
        {

           /* var arr1 = $startValue.split('-');
            var startTime = new Date(arr1[0],arr1[1],arr1[2]);
            var startTimes = startTime.getTime();

            var arr2 =  $endValue.split('-');
            var endTime = new Date( arr2[0],arr2[1],arr2[2]);
            var endTimes = endTime.getTime();*/
            var $startTime = new Date($startValue.replace("-", "/").replace("-", "/"));
            var $endTime = new Date($endValue.replace("-", "/").replace("-", "/"));

            if($endTime<$startTime)
            {
                alert('结束时间不能小于开始时间');
            }
        }


    }


    function addItemList()
    {

        var vTb=$("#TbData");
        var vNum = $("#TbData tr").filter(".ClassRow").size()+1;
        var vTr = $("#TrDataRow1");
         vTrClone = vTr.clone(false,false);
         vTrClone[0].id = "TrDataRow"+vNum;
        var vTrCloneElement = "#TrDataRow"+vNum;
        vTrClone.appendTo(vTb);
        $("#TrDataRow"+vNum+">td>label").html(vNum);
        $("#TrDataRow"+vNum+">td>.forever").attr('id','fore_'+vNum);
        $("#TrDataRow"+vNum+">td>.temp").attr('id','temp_'+vNum);
        $("#TrDataRow"+vNum+">td>.startAt").attr('id','s_'+vNum);
        $("#TrDataRow"+vNum+">td>.endAt").attr('id','e_'+vNum);
        $("#TrDataRow"+vNum+">td>#fore_"+vNum).attr('checked',false);
        $("#TrDataRow"+vNum+">td>#temp_"+vNum).attr('checked',false);

        console.log($("#TrDataRow"+vNum+" "+"#itemName_1").attr('id'));
    }

</script>
<p></p>
@include('/layouts/footer')