@include('/layouts/header')

<h4>创建菜肴</h4>
{{--<div>
<div><label>餐厅名</label><input name="name" type="text" value="{!! $value['menu']['name'] !!}" disabled="true"></div>
<div><label>联系电话</label><input name="phone" type="text" value="{!! $value['menu']['phone'] !!}" disabled="true"></div>
<div><label>餐单网址</label><input name="url" type="text" value="{!! $value['menu']['url'] !!}" disabled="true"></div>
<div><label>餐单开始时间</label><input name="start_at" type="text" value="{!! $value['menu']['start_at'] !!}" disabled="true"></div>
<div><label>餐单结束时间</label><input name="end_at" type="text" value="{!! $value['menu']['end_at'] !!}" disabled="true"></div>
</div>
<br/>--}}
<div class="menuList"><label>餐单号ID: {!! $value['menu']['id'] !!}</label>||<label>餐厅: {!! $value['menu']['name'] !!}</label>||<label>日期: {!! $value['menu']['start_at'] !!}</label>||
    <label>电话: {!! $value['menu']['phone'] !!}</label></div>



<form action="/admin/menu/addItem" method="post">
    <input type="hidden" name="restaurant_id" value="{!! $value['menu']['id'] !!}">
    {!! csrf_field() !!}
<table id="TbData">
    <thead><th><input class="allSelect" type="checkbox">全选</th><th>菜肴名</th><th>价格</th><th>分类</th><th>有效时间</th><th>有效时间段</th></thead>

    <tbody>
    @if(isset($value['item']))
        @for($i=0;$i<count($value['item']);$i++)

<tr class="ShowRow">
    <td><input class="selectItem" type="checkbox" disabled="true" ></td>
    <td><label>{!! $value['item'][$i]['name'] !!}</label></td>
    <td><label>{!! $value['item'][$i]['price'] !!}</label></td>
    <td><label>{!! isset($value['item'][$i]['category_to_many']['name'])?$value['item'][$i]['category_to_many']['name']:"无" !!}</label></td>
    <td><label>@if($value['item'][$i]['status']==2)长期有效 @else 限制时间 @endif</label></td>
    @if($value['item'][$i]['status']==1)
    <td>
        <input   name="old_start_at" type="date" disabled="true"  value="{!! $value['item'][$i]['start_at']!!}"/>至
        <input   name="old_end_at" type="date"   disabled="true" value="{!! $value['item'][$i]['end_at']!!}"/>
        <br/>

    </td>
        @else
        <td>
            <label class="notTime">----至----</label>
            <br/>
        </td>
        @endif

        </tr>
        @endfor
    @endif
    <tr class="ClassRow" id="TrDataRow1">
        <td><input class="selectItem" type="checkbox" value="1" checked="true"></td>
        <td><input name="itemName[]" type="text"/></td>
        <td><input name="itemPrice[]" type="text"/></td>
        <td>
            <select name="category[]" >
                <option>&nbsp;&nbsp;</option>
                @foreach($value['category'] as $category)
                    <option value="{!! $category['id']!!}">{!! $category['name'] !!}</option>
                @endforeach
            </select>
        </td>
        <td><input id="fore_1" class="forever" name="forever[]" type="checkbox" value="2"/>长期有效<br/>
            <input id="temp_1" class="temp"  name="forever[]" type="checkbox" value="1" />限制时间</td>
        <td>
            <input id="s_1"  name="start_at[]" type="date"  />至
            <input id="e_1"  name="end_at[]" type="date"    />
            <br/>

        </td>
    </tr>
    </tbody>

</table>
<div><input  class="addItem"  type="button"  value="添加菜肴"/><input  class="delItem"  type="button"  value="删除选中菜肴"/>
    <input  class="submit"  type="button"  value="提交"/>
</div>
</form>
<div></div>
<script type="text/javascript">
     var vNum= 1;

     $("div").on('click','.submit',function(){

         validateIsValue("input[name='itemName[]']");
         validateIsValue("input[name='itemPrice[]']");
         validateIsValue("select[name='selectName[]']");
         validateIsChecked(".forever");
         $("form").submit();

     });

     //添加行
    $('div').on('click','.addItem',function(){

        validateIsValue("input[name='itemName[]']");
        validateIsValue("input[name='itemPrice[]']");
        validateIsValue("select[name='selectName[]']");
        validateIsChecked(".forever");

        if(vNum>10)
        {
            $(".comment").html('最多添加10条');
            $(".comment").show().delay(3000).hide(0);
            exit;
        }

        var vTb=$("#TbData");
        vNum =vNum+1;
        var vTr = $("<tr class='ClassRow' ></tr>");
        vTr.append("<td><input class='selectItem' type='checkbox' value='"+vNum +"'></td>");
        vTr.append(" <td><input name='itemName[]' type='text'/></td> <td><input name='itemPrice[]' type='text'/></td>");
        vTr.append("<td><select name='category[]'> <option>&nbsp;&nbsp;</option>@foreach($value['category'] as $category)<option value='{!! $category['id']!!}'>{!! $category['name'] !!}</option>@endforeach</select></td>");
        vTr.append("<td><input id='fore_"+vNum+"' class='forever' name='forever[]' type='checkbox' value='2' />长期有效<br/> <input id='temp_"+vNum+"' class='temp' name='forever[]' type='checkbox' value='1' />限制时间</td>");
        vTr.append( "<td><input id='s_"+vNum+"'  name='start_at[]' type='date'     />至<input id='e_"+vNum+"'  name='end_at[]' type='date'   /></td>");
        vTb.append(vTr);
        $(".allSelect").prop('checked',false);

    });

    //判断时间先后,结束时间不能小于开始时间;
     $(document).on('change',"td input[type='date']",function(){
        var $ID = $(this).attr('id').slice(2);

        var $startValue = $("#s_"+$ID).val();
        /* var $startValueString = $startValue.toString()*/
        var $endValue = $("#e_"+$ID).val();

        if(($startValue)&&($endValue))
        {

            /* var arr1 = $startValue.split('-');
             var startTime = new Date(arr1[0],arr1[1],arr1[2]);
             var startTimes = startTime.getTime();

             var arr2 =  $endValue.split('-');
             var endTime = new Date(arr2[0],arr2[1],arr2[2]);
             var endTimes = endTime.getTime();*/
            var $startTime = new Date($startValue.replace("-", "/").replace("-", "/"));
            var $endTime = new Date($endValue.replace("-", "/").replace("-", "/"));

            if($endTime<$startTime)
            {
                $("#e_"+$ID).val(" ");
                $(".comment").html('结束时间不能小于开始时间');
                $(".comment").show().delay(3000).hide(0);
            }
        }

    });

    //长期有效时,无法选择时间
     $(document).on('click',"td .forever",function(){
        var $foreverID = $(this).attr('id').slice(5);

        if($(this).is(':checked'))
        {
            $("#temp_"+$foreverID).attr('checked',false);
        }
        //var $foreverID = id.slice(5);
        $("#s_"+$foreverID).val(" ");
        $("#e_"+$foreverID).val(" ");
        $("#s_"+$foreverID).attr('readonly',readonly);
        $("#e_"+$foreverID).attr('readonly',readonly);
    });

    //限制时间时,可以选择时间;
    $(document).on('click',"td .temp",function(){
        var $tempID = $(this).attr('id').slice(5);

        if($(this).is(':checked'))
        {
            $("#fore_"+$tempID).attr('checked',false);
        }

        $("#s_"+$tempID).removeAttr('readonly');
        $("#e_"+$tempID).removeAttr('readonly');
    });

    $(document).on('change',"th .allSelect",function(){

        if($("th .allSelect").prop('checked'))
        {
            $(".selectItem").prop('checked',true);
        }
        else
        {
            $(".selectItem").prop('checked',false);



        }
    });

    $(document).on('click',".delItem",function(){
        if($(".selectItem").is(':checked'))
        {
            $(".selectItem:first").prop('checked',false);

           $(".selectItem").each(function(index,element){

                if($(element).is(":checked"))
                {

                    $(element).parent().parent().empty();
                }
           })
            $(".allSelect").prop('checked',false);
        }
        else
        {
            alert("请选择至少一份菜肴")
        }
    });
//验证单元格是否为空,空则停止;
    function validateIsValue(selector)
    {
        $(selector).each(function(index,element){
            if($(element).val() == "")
            {
                $(element).css("border-color", "red");
                $(".comment").html('不能为空!');
                $(".comment").show().delay(2000).hide(0);
                exit;
            }
        });
    }

     function validateIsChecked(selector)
     {

         $(selector).each(function(index,element){

             $ID = $(element).attr('id').slice(5);
             if((!$(element).is(":checked")) && (!$("#temp_"+$ID).is(":checked")))
             {

                 $(".comment").html('有效时间不能为空!');
                 $(".comment").show().delay(3000).hide(0);
                 exit;
             }

           /*  if($("#temp_"+$ID).is(":checked") && (($("#s_"+$ID).val()=="")||($("#e_"+$ID).val()=="")))
             {
                 $("#s_"+$ID).css("border-color", "red");
                 $("#e_"+$ID).css("border-color", "red");
                 $(".comment").html('有效时间段不能为空!');
                 $(".comment").show().delay(3000).hide(0);
                 exit;
             }*/

         });
     }

   //点击时移除红色边框;
    $(document).on('click',"input",function(){

        $(this).removeAttr("style");
    });
</script>
<style type="text/css">
    .comment {font-size: 12px;color:red;}
    .addItem {margin:10px;}
    .menuList label {margin:15px;}
    td {text-align:center;vertical-align:middle;}
    </style>
<p class="comment"></p>
@include('/layouts/footer')