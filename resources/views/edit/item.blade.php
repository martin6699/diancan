@include('layouts/header')

<h4>修改菜肴</h4>
<form id="editItem" action="/admin/item/{!! $value[0]['id'] !!}}/edit" method="post">
    {!! csrf_field() !!}
    <table><thead><th>菜肴</th><th>单位</th><th>价格</th><th>开始时间</th><th>结束时间</th></thead>
        <tbody>
        <tr>
            <th><input name="name" type="text" value = "{!! $value[0]['name'] !!}" /></th>
            <th><input name="unit" type="text" value = "{!! $value[0]['unit'] !!}"/></th>
            <th><input name="price" type="text" value = "{!! $value[0]['price'] !!}"/></th>
            <th><input name="start_at" type="datetime" value="{!! $value[0]['start_at'] !!}" /></th>
            <th> <input name="end_at" type="datetime" value="{!! $value[0]['end_at'] !!}"/></th>

        </tr>
        </tbody>
    </table>
<input type="submit" value="提交"/>
{!! $errors->first('price') !!}<br/>
    {!! $errors->first('end_at') !!}

</form>

@include('layouts/footer')
