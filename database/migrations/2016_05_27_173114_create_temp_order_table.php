<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dish_temp_order', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('menu_time');
            $table->integer('menu_id');
            $table->string('menu_name');
            $table->string('item_first');
            $table->decimal('price_first',6,2);
            $table->string('item_second');
            $table->decimal('price_second',6,2);
            $table->integer('user_id');
            $table->string('user_name');
            $table->softDeletes();
            $table->string('deleter');
            $table->uuid('uuid');
            $table->timestamps();
            $table->index(['menu_time','user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dish_temp_order');
    }
}
