<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dish_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('menu_id');
            $table->integer('item_id');
            $table->integer('num');
            $table->decimal('t_price',6,2);
            $table->integer('ins_item_id');
            $table->integer('ins_num');
            $table->decimal('ins_price',6,2);
            $table->integer('act_item_id');
            $table->integer('act_num');
            $table->decimal('act_price',6,2);
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dish_orders');
    }
}
