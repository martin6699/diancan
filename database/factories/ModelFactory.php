<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\DishItem::class, function (Faker\Generator $faker) {
    return [
        'name'=>$faker->name,
        'unit'=>'份',
        'price'=>random_int(12,30)+1.12,
        'status'=>0,
        'created_at'=> $faker->date('Y-m-d','now'),
        'updated_at'=>$faker->dateTimeBetween('2016-01-01','now'),
        'start_at' => $faker->dateTimeBetween('2015-12-01','now'),
        'end_at' => $faker->dateTimeBetween('2015-12-20','now'),
    ];
});
$factory->define(App\DishMenu::class,function(Faker\Generator $faker){
    return [
        'restaurant_name' => $faker->company,
        'phone' => $faker->phoneNumber,
        'picture_url' => $faker->url,
        'start_at'=> $faker->dateTimeBetween('2016-01-01','now'),
        'end_at'=> '2016-01-28',
        'status'=>0,
        'created_at'=>$faker->dateTimeBetween('2016-01-01','now'),
        'updated_at'=>$faker->dateTimeBetween('2016-01-01','now'),

    ];
});