<?php

use Illuminate\Database\Seeder;

class ItemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
       factory(App\DishItem::class,10)
           ->create()
           ->each(function($i){
               $i->MenuToMany()->save(factory(App\DishMenu::class)->make());
           });
    }
}
