<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Permission
 * @package App
 *
 * 定义权限模型 多对多角色表
 */
class Permission extends Model
{
    //

    protected $table = 'permissions';
    protected $fillable = ['name','label','description','created_at','updated_at'];
//权限与角色 多对多
    public function roles()
    {
        return $this->belongsToMany('App\Role','permission_role');

    }
//确认权限是否存在于权限表中
    public function existPermissions($permission)
    {
        //take(1)相当于 limit 1
      $permission_id = $this->select('id')->where('name',$permission->name)->take(1)->get()->toArray();


       if(empty($permission_id))
       {
           return false;
       }
        else
        {
            return true;
        }
    }
}
