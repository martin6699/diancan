<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Permission;
use Illuminate\Support\Collection;

/**
 * Class Role
 * @package App
 *
 * 定义角色模型,多对多权限表;
 * 角色模型中 判断角色是否有权限,添加权限给角色,删除权限等功能
 */
class Role extends Model
{
    protected $table = 'roles';

    public function permissions()
    {
        return $this->belongsToMany('App\Permission','permission_role');

    }

    /**
     * @param $permission
     *
     * 给角色添加权限
     * @return bool
     */
    public function addPermissionTo($permission)
{
    //$permission为模型Permission类的对象;
  if((!$this->permissions()->get()->isEmpty())&&$this->permissions()->get()->contains('id',$permission->id))
    {

        return true;
    }

    return $this->permissions()->save($permission);//把$permission模型对象数据保持进数据库


}

    public function existRole($role)
    {
        $role_id = Role::select('id')->where('name',$role)->take(1)->get()->toArray();

        if(empty($role_id))
        {
            return false;
        }
        else
        {
            return true;

        }


    }

}
