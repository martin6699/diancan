<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];


    public function getId()
    {

        return $this->id;

    }

    public function roles()
    {
        return $this->belongsToMany('App\Role', 'user_role');
    }



    /**
     * @param $role
     * 判断用户是否拥有角色
     * @return bool
     */
    public function hasRole($role)
    {
        //判断$role是不是整数,如果不是,就是字符串或者集合,数组
        if(is_int($role))
        {
            return $this->roles()->get()->contains('id',$role);
        }

        //判断$role是不是字符串,如果不是,就是集合或数组
        if (is_string($role)) {
            //角色表内连接关联权限表 得到一个集合,在集合里使用contains方法查询字段name中是否有$role,有则返回true,没有返回false;
            return $this->roles()->get()->contains('name', $role);
        }
        //!!双感叹号 是三元运算,如 !!a 相当于 a?true:false;
        return !!$role->intersect($this->roles()->get())->count();

    }


//先用权限,再有角色,之后才能给用户分配角色;
//判断该用户是否有权限,其实是判断该用户是否拥有该角色,该角色必须有该权限;
//$permission->roles()返回拥有该$permission权限的角色集合;
    /**
     * @param $permission
     * 先用权限,再有角色,之后才能给用户分配角色;
     * 判断该用户是否有权限,其实是判断该用户是否拥有该角色,该角色必须有该权限;
     * $permission->roles()返回拥有该$permission权限的角色集合;
     * @return bool
     */
    public function hasPermission($permission)
    {
        return $this->hasRole($permission->roles()->get());
    }


    /**
     * @param $role
     * 给用户分配角色
     * @return Role Model
     */
    public function assignRole($role)
    {
        if (is_int($role)) {

            return $this->roles()->save(
                Role::where('id', $role)->firstOrFail()
            );
        } else {
            return $this->roles()->save(
                Role::where('name', $role)->firstOrFail()
            );
        }

    }

}
