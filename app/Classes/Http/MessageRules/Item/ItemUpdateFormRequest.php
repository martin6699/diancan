<?php
namespace App\Classes\Http\MessageRules\Item;

use Illuminate\Foundation\Http\FormRequest;

class ItemUpdateFormRequest extends FormRequest
{
    public function messages()
    {
        return [
            'required' => ':attribute字段必须填写',
            'not_in' => ' :attribue必须是中文,不含types: :values等',
            'numeric' => ' :attribue必须为数字',
            'between' => ' :attribute 范围必须在 :min - :max ',
            'date' => ' :attribute 必须为日期 ',
            'after' => '必须为日期且开始时间小于结束时间',
            'max' => '最大值为 :attribute',

        ];
    }



    public function rules()
    {
        return [
            'name'=>'required|not_in:alpha_dash,array|max:255',
            'unit'=>'required|not_in:alpha_dash,array',
            'price'=>'required|numeric|between:0.01,50.00|max:50',
            'start_at' => 'required|date|after:tomorrow',
            'end_at' => 'required|date|after:start_at',

        ];
    }

    public function authorize()
    {
        return true;
    }
}

