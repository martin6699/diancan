<?php
/**
 * Created by PhpStorm.
 * User: martin6699
 * Date: 2/16/16
 * Time: 09:29
 */

namespace App\Classes\Http\MessageRules\Author;

use Illuminate\Foundation\Http\FormRequest;

class RoleFormRequest extends FormRequest
{

    public function Messages()
    {
      return [
           'required' => ':attribute字段必须被填写',
           'unique' => '该角色已被注册,请重新创建!',
      ];
    }

    public function rules()
    {
        return [
            'name' => 'required|unique:roles,name',
            'label' => 'required',
            'description' => 'required',
        ];

    }

    public function authorize()
    {
        return true;
    }
}