<?php
namespace App\Classes\Http\MessageRules\Menu;

use Illuminate\Foundation\Http\FormRequest;

class CreateMenuFormRequest extends FormRequest
{
    public function messages()
    {

        return [

            'integer' => ':attribute 必须为整数',
            'numeric' => ':attribute 必须为数字',
            'unique' => '不能重复提交相同日期餐单',
            'required' => ':attrbute 必须填写',

        ];

    }


    public function rules()
    {

       return [

            'name' => 'required|not_in:alpha_dash,array|max:255',
            'phone' => 'required|numeric',
            //'price' => 'required|numeric|between:0.01,50.00|max:50',
            'start_at' => 'required|date',
            'end_at' => 'required|date|after:start_at',
        ];




    }

    public function authorize()
    {
        return true;
    }

}
