<?php
namespace App\Classes\Http\MessageRules\Auth;

use Illuminate\Foundation\Http\FormRequest;

class LoginFormRequest extends FormRequest
{
    public function messages()
    {
        return [
            'required' => ':attribute字段必须填写',
            'email' => '必须是邮箱',
            'min' => '密码至少六位',
        ];
    }


    public function rules()

    {

        return [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ];
    }

    public function authorize()
    {
        return true;

    }
}