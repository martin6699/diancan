<?php
namespace App\Classes\Http\MessageRules\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RegisterFormRequest extends FormRequest
{
    public function messages()
    {
       return [
           'required' => ':attribute字段必须填写',
           'not_in' => ' :attribute必须是中文,不含types: :values等',
           'unique' => '该邮箱被注册!',
           'confirmed' => '密码不相同',
           'min' => '至少8位密码',
       ];
    }


    public function rules()
    {

        return [
            'name' => 'required|not_in:alpha_dash,array|max:255',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed|min:8',
            'password_confirmation' => 'required',
        ];
    }

    public function authorize()
    {
      return true;

    }
}