<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

use Auth;

class SendDishOrder extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $user;
    public $product;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($item1,$price1,$item2,$price2)
    {
        //
        $this->user = Auth::User()->getId();
        $this->product[1] = array($item1,$price1);
        $this->product[2] = array($item2,$price2);



    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //先放着



    }
}
