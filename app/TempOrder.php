<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TempOrder extends Model
{
    use softDeletes;

    protected $table = "dish_temp_order";

    protected $dates = ['deleted_at'];


}
