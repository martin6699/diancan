<?php

namespace App\Handlers\Commands;

use App\Commands\DiancanDoCommand;
use Illuminate\Queue\InteractsWithQueue;

class DiancanDoCommandHandler
{
    /**
     * Create the command handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the command.
     *
     * @param  DiancanDoCommand  $command
     * @return void
     */
    public function handle(DiancanDoCommand $command)
    {
        //
    }
}
