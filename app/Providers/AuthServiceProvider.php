<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Permission;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        //定义角色表roles,权限表permissions,角色对应权限表permission_role,角色(可以理解为群组)对应用户表user_role;
        //一个角色有多个权限;一个用户有多个角色;
        $permissions = Permission::with('roles')->get();
        foreach($permissions as $permission)
        {
            $gate->define($permission->name, function($user) use ($permission){
                return $user->hasPermission($permission);
            });
        }
    }
}
