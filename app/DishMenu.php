<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class DishMenu extends Model
{
    //菜单

    protected $table = 'dish_menu';
    protected $guarded = ['restaurant_name','phone','picture_url','start_at','end_at'];

    public static function displayMenu()
    {
        //return self::where('end_at','>=',Carbon::now())->toArray();
        return self::select('id','phone')->get()->toArray();
    }

    public function insertValue($query)
    {
        $this->restaurant_name =$query['restaurant_name'];
        $this->phone = $query['phone'];
        $this->picture_url = $query['picture_url'];
        $this->start_at = $query['start_at'];
        $this->end_at = $query['end_at'];
        $this->save();
    }


    public function updateValue($query,$id)
    {
        $this->find($id)->update($query);
    }

    public function deleteMenu($id)
    {
        $this->find($id)->delete();
    }

    //餐单与菜肴的多对多关联
    public function ItemToMany()
    {
        return $this->belongsToMany('App\DishItem','dish_menu_item_map','dish_menu_id','dish_item_id');
    }
}
