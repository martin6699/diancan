<?php

namespace App\Http\Controllers\Admin;

use App\Permission;
use App\Role;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Classes\Http\MessageRules\Author\RoleFormRequest;
use App\User;
use Illuminate\Support\Facades\DB;


class AuthorizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $permission;

    public function index()
    {
        //
    }


    public function addPermission(Request $request)
    {
        //
        $permissions = Permission::all();
        $isExist = $permissions->contains('name', $request->input('name'));

        //判断新增权限是否存在于权限表中
        if ($isExist) {
            return view('/admin/author/successPer')->with('permission', $this->getPermissionArray());
        }
        //往数据库中加权限
        $permission = Permission::create([
            'name' => $request->input('name'),
            'label' => $request->input('label'),
            'description' => $request->input('description'),
            'create_at' => Carbon::now(),
            'update_at' => Carbon::now(),
        ]);
        if (!$permission) {
            return false;
        } else {

            return view('/admin/author/successPer')->with('permission', $this->getPermissionArray());
        }


    }

    /* public function setPermission($permission)
     {
         if (!in_array($permission, $this->$permission)) {
             $this->permission[] = $permission;
         }
     }

     public function getPermission()
     {
         if (empty($this->permission)) {
             return false;
         } else
             return $this->permission;

     }*/

    /**
     * 返回权限表数据
     * @return array
     */
    public function getPermissionArray()
    {
        return Permission::all()->toArray();
    }


    public function getRoleArray()
    {
        return Role::select('id', 'name', 'label', 'description')->get()->toArray();
    }


    //返回一个创建角色的表单视图
    public function addRole()
    {

        $permissionGroup = $this->getPermissionGroup();

        return view('admin.author.addRole')->with('permissions', $permissionGroup);

    }


    public function storeRole(RoleFormRequest $request)
    {
        //创建新角色类
        $role = new Role;
        $role->name = $request->input('name');
        $role->label = $request->input('label');
        $role->description = $request->input('description');
        $role->save();

        //获取权限集合数组
        $permissions = $request->input('permissions');

        //角色分配权限,并存入数据库;
        for ($i = 0; $i < count($permissions); $i++) {
            $permission = Permission::find($permissions[$i]);

            if ($permission == null) {

                return false;
            }

            $role->addPermissionTo($permission);
        }


        return redirect()->route('displayRole');


    }

    //显示角色列表
    public function displayRole()
    {
        return view('/admin/author/displayRole')->with('roles', $this->getRoleArray());
    }


    public function editRole()
    {
        $role = Role::find(19);
        $selectedPermissions = $role->permissions()->get();
        dd($selectedPermissions);

    }

    //获取所有权限组
    public function getPermissionGroup()
    {
        $permissions = Permission::select('id', 'description', 'group')->get();

        $permissionGroup = $permissions->groupBy('group');

        $permissionGroup = array_values($permissionGroup->toArray());
        return $permissionGroup;
    }


    /**
     *
     * return 数组(该数组包括用户基本信息,单一的用户角色;)
     */
    public function displayUser()
    {
        $user = User::select('users.id', 'users.name', 'users.email', 'roles.name as currentRole')->
        leftjoin('user_role', 'users.id', '=', 'user_role.user_id')->
        leftjoin('roles', 'user_role.role_id', '=', 'roles.id')->get()->toArray();

        $role = Role::select('id', 'name')->get()->toArray();

        $userAndRole = array(
            0 => $user,
            1 => $role,
        );
        return view('admin.author.userRole')->with('users', $userAndRole);

    }

    /**
     * @param Request $request
     * 添加角色给用户
     * @return int
     */
    public function addRoleToUser(Request $request)
    {

        $userID = (int)$request->input('userID');
        $selectID = (int)$request->input('selectID');

        $user = User::find($userID);

        if ($user->hasRole($selectID)) {
            return 1;
        } elseif ($user->roles()->get()->isEmpty()) {
            $user->assignRole($selectID);
            return 1;
        } else {


            DB::table('user_role')->where('user_id',$userID)->update(['role_id' => $selectID]);

            return 1;

        }

        return 0;


    }


    public function destroy(Request $request)
    {
        $deleteID = $request->input('deleteID');
        $user = User::find($deleteID);
        $user->delete();
        return 1;


    }
}
