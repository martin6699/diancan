<?php

namespace App\Http\Controllers\Menu;

use App\DishItem;
use App\DishMenu;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Classes\Http\MessageRules\Menu as MenuMessage;
use App\Classes\Http\MessageRules\Item;
use Illuminate\Support\Facades\Auth;
class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {



        //$item = DishItem::displayMenuAll();

      /* $menu = new DishMenu();
        $menuQuery = $menu->where('status',0)->orderBy('id','desc')->first();
        $itemd = $menuQuery->ItemToMany()->get();*/


        $dt = Carbon::now();
        $year = $dt->year;
        $month = $dt->month;



        $menu = DishMenu::has('ItemToMany')->where("status",1)->whereYear('start_at','=',$year)->whereMonth('start_at','>=',$month)->orderBy('updated_at','desc')->first();//獲取有效狀態下最後一個菜單
       if($menu == null)
       {
           return view('display.item')->with('value',0);
       }

        $itemDisplay = $menu->ItemToMany()->paginate(3);//关联到中间表后返回的菜肴item信息;

        $menuDisplay = DishMenu::has('ItemToMany')->select('id','restaurant_name')->where("status",1)->whereYear('start_at','=',$year)->whereMonth('start_at','>=',$month)->orderBy('updated_at','desc')->get()->toArray();
       // $arrayValue = array_first($menuDisplay,function($key,$value){return $value['id']==1;});


        $value = array('menu'=>$menuDisplay,'item'=>$itemDisplay,'selectID'=>0);//menu展示菜单选项;item展示菜单对应的菜肴列表;selectID默认值为0 便于分页传值
        return view('display.item')->with('value',$value);

       


        //输出mysql执行语句
       // app('db')->enableQueryLog();
       // $item = DishMenu::displayMenu();
       // dd(app('db')->getQueryLog());

    }

    /**根据餐单选择,返回相应餐单的菜肴
     *
     */
    public function select(MenuMessage\MenuIdFormRequest $request)
    {



        $selectID = $request->input('selectID');
        $dt = Carbon::now();
        $year = $dt->year;
        $month = $dt->month;
        $menu = DishMenu::where('status',1)->where('id',$selectID)->first();//得到一条关于$selectID的完整记录;
        $itemDisplay = $menu->ItemToMany()->paginate(3);//关联到中间表后返回的菜肴item信息;
        $menuDisplay = DishMenu::select('id','restaurant_name')->where("status",1)->whereYear('start_at','=', $year)->whereMonth('start_at','=',$month)->get()->toArray();
        //将数组里面的某个指定的值置换到数组开头位置;
        foreach($menuDisplay as $key => $value)
        {
            if($value['id']==$selectID&&$key!=0)
            {
                $replace = $menuDisplay[0];
                $menuDisplay[0] = $menuDisplay[$key];
                $menuDisplay[$key] = $replace;
            }
        }
        $value = array('menu'=>$menuDisplay,'item'=>$itemDisplay,'selectID'=>$selectID);

       return view('display.item')->with('value',$value);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
     $item = DishItem::select('id','name','unit','price','start_at','end_at')->where('status','<>',0)->where('id',$id)->get()->toArray();
        if($item === null){
            return ;
        }

        return view('edit.item')->with('value',$item);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Item\ItemUpdateFormRequest $request,$id)
    {
        //

/*
        $this->validate($request,[
            'name'=>'required|Not In:alpha_dash,array|max:255',
            'unit'=>'required|Not In:alpha_dash,array',
            'price'=>'required|numeric|between:0.01,50.00|max:50',
            'start_at' => 'required|date|after:tomorrow',
            'end_at' => 'required|date|after:start_at',
        ]);*/

        $name = $request->input('name');
        $unit = $request->input('unit');
        $price = $request->input('price');
        $start_at = $request->input('start_at');
        $end_at = $request->input('end_at');

        $update = DishItem::where('id',$id)->update(['name' => $name,'unit' => $unit,'price' => $price,'updated_at' => Carbon::now(),'start_at' => $start_at,'end_at' =>$end_at]);
        if($update!=1)
        {
            return false;
        }
        else
        {
            return $this->index();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //

        $itemID = $request->input('deleteID');

       $item = DishItem::find($itemID);
        if($item === null){
            return ;
        }
        //dd($item->MenuToMany);
        $item->MenuToMany()->detach();

        $item->delete();


        //$it->delete();
        return "sucess$itemID";


    }


}
