<?php

namespace App\Http\Controllers\Menu;


use App\DishCategory;
use App\DishItem;

use App\DishMenu;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use App\Classes\Http\MessageRules\Menu\CreateMenuFormRequest;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected static $values;

    public function index()
    {
        //


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(CreateMenuFormRequest $request)
    {



        $IsMenu = DishMenu::select('id')->where('restaurant_name', $request->input('name'))
            ->where('start_at', $request->input('start_at'))
            ->where('end_at', $request->input('end_at'))->count();

        if ($IsMenu > 0) {
            $message = "不能重复添加相同餐单!需要修改,请到餐单修改栏!";
            return view("/createMenu/menu")->with('message', $message);
        }


        //单例模式 instanceof 判断是否为DishMenu对象
        if (self::$values instanceof DishMenu) {
            $menu = self::$values;
        } else {

            self::$values = new DishMenu();
            $menu = self::$values;

        }

        $menu->restaurant_name = $request->input('name');
        $menu->phone = $request->input('phone');
        $menu->picture_url = $request->input('restUrl');
        $menu->start_at = $request->input('start_at');
        $menu->end_at = $request->input('end_at');
        $menu->status = 1;
        $menu->save();

        $start_at = date('Y-m', strtotime($menu->start_at));


        $menuArray = [
            'id' => $menu->id,
            'name' => $menu->restaurant_name,
            'phone' => $menu->phone,
            'start_at' => $start_at,
        ];


        $category = DishCategory::select('id', 'name', 'parent_id')->get()->toArray();

        $menuValue = ['menu' => $menuArray, 'category' => $category];

        return view('/createMenu/item')->with('value', $menuValue);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $restaurant_id = $request->input('restaurant_id');

        $itemArray = $request->input('itemName');

        $priceArray = $request->input('itemPrice');

        $categoryArray = $request->input('category');

        $foreverArray = $request->input('forever');
        $startAtArray = $request->input('start_at');
        $endAtArray = $request->input('end_at');
        $itemIdArray = $this->batchSaveItemToMenu($itemArray, $priceArray, $foreverArray, $startAtArray, $endAtArray);
        //$itemStatusArray = DishItem::select('status')->whereIn('id',$itemIdArray)->get()->toArray();


        $itemsArray = DishItem::select('id','status')->whereIn('id',$itemIdArray)->get()->toArray();
        $itemIdStatus = array();

        for($i=0;$i<count($itemsArray);$i++)
        {
            $id = $itemsArray[$i]['id'];

            $itemIdStatus[$id] = array('status'=>$itemsArray[$i]['status']);
        }

        $menu = DishMenu::find($restaurant_id);

        $menu->ItemToMany()->attach($itemIdStatus);

        for ($i = 0; $i < count($itemIdArray); $i++) {
            $object = dishItem::find($itemIdArray[$i]);

            $object->CategoryToMany()->attach($categoryArray[$i]);

        }

        return redirect('/admin/menu/show');

    }

    //把批量菜肴加到菜肴表里面 返回菜肴id和status数组
    public function batchSaveItemToMenu($name, $price, $status, $start_at, $end_at)
    {
        $itemIdArray = "";

        for ($i = 0, $j = 0; $i < count($name); $i++) {
            $item = New DishItem();
            $item->name = $name[$i];
            $item->unit = "份";
            $item->price = $price[$i];
            $item->status = $status[$i];

            if ($status[$i] == 1) {
                $item->start_at = $start_at[$j];
                $item->end_at = $end_at[$j];
                $j++;
            }

            $item->created_at = Carbon::now();
            $item->updated_at = Carbon::now();
            $item->save();
            $itemIdArray[] = $item->id;

        }



        return $itemIdArray;

    }

    //给菜肴添加分类
    public function SaveItemToCategory($name, $price, $status, $start_at, $end_at)
    {

    }


    /**
     * @return $this
     */
    public function show()
    {

        $menu = DishMenu::select('id', 'restaurant_name', 'phone', 'picture_url', 'start_at', 'end_at')->orderBy('start_at', 'desc')->get()->toArray();

        return view('update.menu')->with('value', $menu);




    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        /* $menu =DishMenu::with('ItemToMany.CategoryToMany')->find($id);
         $item = $menu->ItemToMany()->get();*/
        $menu = DishMenu::find($id);
        $start_at = date('Y-m', strtotime($menu->start_at));


        $menuArray = [
            'id' => $menu->id,
            'name' => $menu->restaurant_name,
            'phone' => $menu->phone,
            'start_at' => $start_at,
        ];


        $category = DishCategory::select('id', 'name', 'parent_id')->get()->toArray();

        if (count($menu->ItemToMany) > 0) {
            //取得关联的菜肴ID
            foreach ($menu->ItemToMany as $item) {
                $itemId[] = ($item->pivot->dish_item_id);
            }
            //获取关联的菜肴信息 包括菜肴对应的分类
            $itemArray = DishItem::with('CategoryToMany')->whereIn('id', $itemId)->get()->toArray();

            for ($i = 0; $i < count($itemArray); $i++) {
                if (count($itemArray[$i]['category_to_many']) > 0) {
                    $id = $itemArray[$i]['category_to_many'][0]['id'];
                    $name = $itemArray[$i]['category_to_many'][0]['name'];
                    $itemArray[$i]['category_to_many'] = array('id' => $id,'name' => $name);
                }

                if($itemArray[$i]['status']==1){
                    $itemArray[$i]['start_at'] = date('Y-m-d', strtotime($itemArray[$i]['start_at']));
                    $itemArray[$i]['end_at'] = date('Y-m-d', strtotime($itemArray[$i]['end_at']));
                }


            }

            $menuValue = ['menu' => $menuArray, 'category' => $category, 'item' => $itemArray];

        } else {
            $menuValue = ['menu' => $menuArray, 'category' => $category];
        }


        return view('/createMenu/item')->with('value', $menuValue);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     */
    public function update($id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $menuID = $request->input('deleteID');
        $menu = DishMenu::find($menuID);

        //删除中间表中所有关联的菜肴;
        $menu->ItemToMany()->detach();

        $menu->delete();

        return "删除成功";


    }

    public function singleton()
    {

    }


}
