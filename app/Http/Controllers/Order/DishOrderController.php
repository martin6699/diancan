<?php

namespace App\Http\Controllers\Order;

use App\DishMenu;
use App\DishItem;
use App\TempOrder;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Uuid;
use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

/**
 * Class DishOrderController
 * @package App\Http\Controllers\Order
 * 点餐管理,包括员工点餐 创建\修改\删除订单;帮别人点餐;
 */
class DishOrderController extends Controller
{

    //订单实际使用人ID;
    protected $owner;
    public function __construct()
    {
 if(Auth::check())
 {
    $this->owner = Auth::User()->getId();
 }
        else
        {
            return redirect('/auth/login');
        }


    }
    //餐单列表
    public function index()
    {
        /*$menuNotIn = DishMenu::select('id')->where('status','<>',0)->whereNotIn('id',function($query){
            $query->select(DB::Raw('id'))->from('dish_menu_item_map')->where('status','<>',0);
        })->get();*/

        //已status状态确定是否可上架;
       $menu = DishMenu::select(DB::raw("id,restaurant_name,phone,picture_url,status,DATE_FORMAT(start_at,'%Y-%m-%d') as 'start_at',DATE_FORMAT(end_at,'%Y-%m-%d') as 'end_at'"))
            ->whereIn('id',function($query){
                $query->select('dish_menu_id')->from('dish_menu_item_map')->where('status','<>',0);
            })
            ->where('status','<>',0)
            ->distinct()
            ->orderBy('start_at','desc')
            ->orderBy('end_at','desc')
            ->get()->toArray();

       /* $menu = DishMenu::select(DB::raw("id,restaurant_name,phone,picture_url,status,DATE_FORMAT(start_at,'%Y-%m-%d') as 'start_at',DATE_FORMAT(end_at,'%Y-%m-%d') as 'end_at'"))
            ->whereIn('id',function($query){
                $query->select(DB::Raw('id'))->from('dish_menu_item_map')->where('status','<>',0);
            })->get()->toArray()
        ;*/
        if(count($menu)==0)
        {
            return view('errors.custom')->with('show','没有菜单');
        }



        return view('order.menuList')->with('values',$menu);

    }


    /**
     * 展示今天员工临时订单列表
     * @return $this
     */
    public function showTodayTempOrder()
    {
        //总餐单列表
       // $orderList = TempOrder::all()->toArray();
        $today = date('Y-m-d',strtotime(Carbon::now()));
        $orderList =  TempOrder::where('menu_time',$today)->get()->toArray();

        if(count($orderList)==0)
        {
            return view('errors.custom')->with('show','今天无人点餐');
        }
        //返回下载图片地址
        //$img = $this->GrabImage("http://imgsrc.baidu.com/baike/abpic/item/6648d73db0edd1e89f3d62f7.jpg", "");

        return view('order.tempOrder')->with('values',$orderList);
    }


    public function showAllOrder()
    {
        $orderList = TempOrder::all()->toArray();
        return view('Order.tempOrder')->with('values',$orderList);
    }
    /**
     * @param Request $request 点餐 生成订单
     * @return $this
     */
    public function store(Request $request)
    {

        $cookies = $request->cookie();
        //需要做判断数据库是否已经有某人某天订了某餐单;有 返回已点餐
        $today = date('Y-m-d',strtotime(Carbon::now()));

        $item = $request->input('item1');//item1数组
        $item_first = $item[0];
        $item_second = $item[1];
        $price1 = $request->input('a_p');
        $price2 = $request->input('b_p');
        $userID = $request->input('userID');
        $menuID = $request->input('menuID');
        $user = User::find($userID)->toArray();
        $userName = $user['name'];
        $menu= DishMenu::find($menuID)->toArray();
        $menuName = $menu['restaurant_name'];

        $tempOrder = TempOrder::where('menu_time',$today)->where('user_id',$userID)->where('menu_id',$menuID)->get()->toArray();
  //如果已有订单则不能再点;
        if(count($tempOrder)>0)
  {
      return view('errors.custom')->with('show','已有订单');
  }
        $order = new TempOrder();
        $order->menu_time = $today;
        $order->menu_id = $menuID;
        $order->menu_name = $menuName;
        $order->item_first = $item_first;
        $order->price_first = $price1;
        $order->item_second = $item_second;
        $order->price_second = $price2;
        $order->user_id = $userID;
        $order->user_name = $userName;
        //基于https://github.com/webpatser/laravel-uuid
        $order->uuid = Uuid::generate();
        $order->save();
        $order = null;
        //$orderList = TempOrder::all()->toArray();
        $orderList = TempOrder::where('menu_time',$today)->get()->toArray();
        return view('order.tempOrder')->with('values',$orderList);


    }

    /**
     * @param $id
     * 筛选:过了本月的菜单 只能显示长期有效地菜肴
     * 根据id确认是否为本月餐单,不是则显示该餐单常见菜肴;
     * @return view
     */
    public function getMenu($id)
    {
        $menu = DishMenu::find($id);
        //$currentMouth = date('Y-m-01',strtotime(Carbon::now()));
        $today = date('Y-m-d',strtotime(Carbon::now()));

        $end_at = date('Y-m-d',strtotime($menu->end_at));
        $item = array();
        $item['menuID'] = $id;


//        if(count($menu->ItemToMany())>0) {
//
//            $itemArray = $menu->ItemToMany->where('status', 2);
//            //得到itemid;
//            foreach ($itemArray as $item) {
//                $itemId[] = $item->pivot->dish_item_id;
//            }
//            app('db')->enableQueryLog();
//            $items = DishItem::with('CategoryToMany')->whereIn('id', $itemId)->get();
//            $zhengcan = [];
//            $dundang = [];
//            foreach ($items as &$item) {
//                if ($item->CategoryToMany->first()->name == 'zhengcan') {
//                    $zhengcan[] = $item;
//                }
//                if ($item->CategoryToMany->first()->name == 'duntang') {
//                    $dundang[] = $item;
//                }
//
//            }
//
//            dd($items);
//            dd(app('db')->getQueryLog());
//
//            for ($i = 0; $i < count($item); $i++) {
//                $item[$i]['category_to_many'] = Array('id' => $item[$i]['category_to_many'][0]['id'], 'name' => $item[$i]['category_to_many'][0]['name']);
//
//            }
     //   }


        //$item = $menu->with('ItemToMany.CategoryToMany')->where('id',$id)->get()->toArray();






        //餐单过期,显示常规菜
        if($end_at<$today)
        {

            $items = DishMenu::find($id)->ItemToMany()->whereRaw('dish_item.status = 2')->get();

            //如果有常规菜式
            if((count($items->toArray())>0)) {

                $item[] = $this->getItemFromCateory($items);

                return view('order.itemList')->with('values',$item);


            }
            else{
                return view("errors.custom")->with('show','无常规菜肴并且菜单过期,出错!');
            }

        }

        else{//餐单没过期,显示全部

            $items = $menu->ItemToMany()->get();


            if(count($items->toArray())>0 && ($this->getItemFromCateory($items)!="not_category"))
            {


                $item[] = $this->getItemFromCateory($items);
                return view('order.itemList')->with('values',$item);

            }

            else
            {
                return view("errors.custom")->with('show','分类出错');
            }


        }




    }




    public function userOrderDelete(Request $request)
    {
        //userID
            $id = $request->input('orderId');

            $delIsOk = TempOrder::where('id',$id)->delete();

        if($delIsOk){

            TempOrder::onlyTrashed()->where('id',$id)->update(['deleter' => Auth::user()->name]);
        }



               $today = date('Y-m-d',strtotime(Carbon::now()));
               $orderList =  TempOrder::where('menu_time',$today)->get()->toArray();

               if(count($orderList)==0)
               {
                   return view('errors.custom')->with('show','今天无人点餐');
               }

               return view('/order/tempOrder')->with('values',$orderList);



    }
    /**
     * @param $itemArray
     * 返回按分类为key,分类对应的菜肴为value的数组;
     * @return $this|array
     */
    public function getItemFromCateory($itemArray)
    {
        foreach ($itemArray as $item) {
            $itemId[] = $item->pivot->dish_item_id;
        }

        $item = DishItem::with('CategoryToMany')->select('id', 'name', 'price')->whereIn('id', $itemId)->get()->toArray();


        for ($i = 0; $i < count($item); $i++) {

            if(count($item[$i]['category_to_many'])==0)
            {

                return "not_category";
            }

            //$item[$i]['category_to_many'] = Array('id' => $item[$i]['category_to_many'][0]['id'],'name' => $item[$i]['category_to_many'][0]['name']);
            $item[$i]['categoryId'] = $item[$i]['category_to_many'][0]['id'];
            $item[$i]['categoryName'] = $item[$i]['category_to_many'][0]['name'];
            unset($item[$i]['category_to_many']);

        }


        $item = collect($item)->groupBy('categoryName')->toArray();


        return $item;
    }


    /**
     * @param $url
     * @param string $filename
     * 获取远端图片 并保存到本地;
     * @return bool|string
     */
    public function GrabImage($url,$filename="")
    {


        if ($url == ""):return false;
        endif;
        //如果$url地址为空，直接退出
        if ($filename == "") {
            //如果没有指定新的文件名
            $ext = strrchr($url, ".");
            //得到$url的图片格式
            if ($ext != ".gif" && $ext != ".jpg"):return false;
            endif;
            //如果图片格式不为.gif或者.jpg，直接退出
            $filename = public_path()."/img/".date("dMYHis") . $ext;
            //用天月面时分秒来命名新的文件名
        }
        ob_start();//打开输出
        readfile($url);//输出图片文件
        $img = ob_get_contents();//得到浏览器输出
        ob_end_clean();//清除输出并关闭
        $size = strlen($img);//得到图片大小
        $fp2 = @fopen("$filename", "a");
        fwrite($fp2, $img);//向当前目录写入图片文件，并重新命名
        fclose($fp2);
        return "$filename";//返回新的文件名



    }

}
