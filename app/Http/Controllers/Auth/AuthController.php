<?php

namespace App\Http\Controllers\Auth;

use App\Classes\Http\MessageRules\Auth\LoginFormRequest;
use App\User;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Guard;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Classes\Http\MessageRules\Auth\RegisterFormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */
    protected $auth;
    protected $user;
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(Guard $auth,User $user)
    {
        $this->auth = $auth;
        $this->user = $user;
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);


    }

    protected function register(RegisterFormRequest $request)
    {


        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'create_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        if(!$user)
        {
            return false;
        }

        else
        {
            return Redirect::to('/auth/login');
        }

    }

    public function getLogin(LoginFormRequest $request)
    {

       $email = $request->input('email');
        $password = $request->input('password');
        $remember = $request->input('remember');
        if(Auth::attempt(['email' => $email , 'password' => $password],$remember))
        {

          return redirect('/item');
        }
        else
        {
          return view('/auth/login')->with('messages','验证失败!用户名或密码不正确');
        }

    }

    public function getLogout()
    {
        $this->auth->logout();
        return redirect('/auth/login');
    }



}

