<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::group(['middleware' => 'auth'],function(){

    Route::get('/','Menu\ItemController@index');

    Route::post('/menu/select', 'Menu\ItemController@select');

    Route::get('/test', function () {
        return view("display.item");
    });

    Route::post('/menu', 'Menu\MenuController@show');
    Route::post('/update', 'Menu\MenuController@update');

    Route::get('/item', 'Menu\ItemController@index');

//Route::get('/admin/{name}/edit/{id}',function($name,$id){return view("edit.$name")->with('name',[$name,$id]); });

    Route::get('/admin/item/{id}/edit', 'Menu\ItemController@edit');//变量id传递给方法edit()作为函数参数
    Route::post('/admin/item/{id}/edit', 'Menu\ItemController@update');

    Route::post('/admin/item/delete', 'Menu\ItemController@destroy');

    Route::get('/menu/select', 'Menu\ItemController@select');

    Route::get('/admin/menu/show','Menu\MenuController@show');
    Route::get('/admin/menu/{id}/edit','Menu\MenuController@edit');
    Route::post('/admin/menu/delete','Menu\MenuController@destroy');


    Route::get('/user/menu/show','Order\DishOrderController@index');
    Route::get("/user/menu/{id}/ordering",'Order\DishOrderController@getMenu');
    Route::post("/user/menu/store",'Order\DishOrderController@store');
    Route::get("/user/temp/order/show",'Order\DishOrderController@showTodayTempOrder');
    Route::get('/user/all/order/show','Order\DishOrderController@showAllOrder');
    Route::post('/user/order/delete','Order\DishOrderController@userOrderDelete');
    Route::get('/user/order/delete','Order\DishOrderController@index');

});


Route::get('/auth/login', function () {
    return view('auth.login');
});

Route::post('/auth/login','Auth\AuthController@getLogin');
Route::post('/auth/register','Auth\AuthController@register');
Route::get('/auth/register', function () {
    return view('auth.register');
});

Route::get('/auth/logout','Auth\AuthController@getLogout');



Route::get('/errors/{id}',function($id){
    return view("errors.$id");
});


Route::get('/admin/permission/add',function(){
    return view("admin.author.createPermission");
});

Route::post('/admin/permission/add','Admin\AuthorizationController@addPermission');

/*Route::get('/admin/author/{addName}', function($addName){
    return view("admin.author.$addName");
});*/


Route::get('/test/blade',function(){ return view('layouts.test');});
Route::get('/test/index','TestController@index');

Route::get('/welcome',function(){return view('welcome');});



//Route::get("/test/index",function(){ return view('welcome');});
Route::get("/test/push",function(){ return view('order.jqueryPush');});


Route::get('/admin/role/add','Admin\AuthorizationController@addRole');

Route::post('/admin/author/storeRole','Admin\AuthorizationController@storeRole');
Route::get('/admin/author/{id}/edit','Admin\AuthorizationController@editRole');

Route::get('/admin/role/displayRole',['as' => 'displayRole', 'uses' => 'Admin\AuthorizationController@displayRole']);
Route::get('/admin/user/displayUser','Admin\AuthorizationController@displayUser');

Route::post('/admin/user/addRoleToUser','Admin\AuthorizationController@addRoleToUser');

Route::post('/admin/user/delete','Admin\AuthorizationController@destroy');
Route::get('/admin/menu/create',function(){

    return view("createMenu.menu")->with('message'," ");
});

Route::post('/admin/menu/create','Menu\MenuController@create');
Route::post('/admin/menu/keyCreate','Menu\MenuController@KeyCreate');
Route::post('/admin/menu/addItem','Menu\MenuController@store');

Route::get('/martin/test',function(){
    return base_path();
});






