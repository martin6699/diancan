<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DishCategory extends Model
{
    //分类
    protected $table = 'dish_category';
    protected $guarded = ['name','parent_id'];


    public static function displayCategory()
    {
        return self::all()->toArray();
    }

    public static function scopeDisplayChildren($parent_id)
    {
        return self::where('parent_id',$parent_id)->get()->toArray();
    }

    public function insertValue($query)
    {

        if(!$this->where('name',$query['name']))
        {
            $this->name = $query['name'];
            $this->parent_id = $query['parent_id']?$query['parent_id']:0;
            $this->save();
        }
        else
            return false;

    }

    public function updateValue($id,$query)
    {
        $this->find($id)->update($query);
    }

    public function deleteCategory($id)
    {

        $this->find($id)->delete();
    }

    //分类与菜肴的多对多关联
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function belongsToItem()
    {
        return $this->belongsToMany('App\DishItem','dish_item_category_map','dish_category_id','dish_item_id')->withTimestamps();
    }


}
