<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class DishItem extends Model
{


    protected $table = 'dish_item';
    protected $fillable = ['name', 'unit', 'price', 'status'];


    //返回菜单所有明细
    public static function displayMenuAll()
    {

        return self::where('status',0)->get()->toArray();
    }


    public static function scopeDisplayMenu($query)
    {
        return $query->where('status', '=', 0)->get()->toArray();
    }

    public  function insertValue($query)
    {
        $this->name = $query['name'];
        $this->unit = $query['unit'];
        $this->price = $query['price'];
        $this->status = 0;//0表示正常状态,1表示被软删除
        $this->save();


    }


   //菜肴与餐单的多对多关联
    public function MenuToMany()
    {
        return $this->belongsToMany('App\DishMenu', 'dish_menu_item_map', 'dish_item_id', 'dish_menu_id')->withTimestamps();
    }

   //菜肴与分类的多对多关联
    public function CategoryToMany()
    {
        return $this->belongsToMany('App\DishCategory','dish_item_category_map','dish_item_id','dish_category_id')->withTimestamps();
    }



}






